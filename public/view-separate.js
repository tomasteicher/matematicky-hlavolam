var lastStep = [];
var noItem = 'X';
var stepIndex = 0;
var steps = [];

// Vizualisation options.
var showNumberRow = true;
var showStepInfoRow = true;
var showNRow = true;
var showResultRow = true;
var showSelectbox = true;

// DOM elements.
var visiblePart = $('#collatz-numbers');
var htmlSamples = $('#samples').first();
var htmlSampleTable = htmlSamples.find(".collatz-numbers");
var htmlLastStep = null;
var htmlNumberRow = htmlSamples.find('tr.number-row').first();
var htmlNRow = htmlSamples.find('tr.n-row').first();
var htmlInfoRow = htmlSamples.find('tr.number-step-info').first();
var htmlResultRow = htmlSamples.find('tr.result-row').first();
var htmlTd = $('<td>/td>');
var htmlSelectbox = htmlSamples.find('select.n-choose').first();
var sampleTrSelectbox = htmlSamples.find('.selectbox-row').first();

// Action to selectbox.
htmlSelectbox.on('change', function () {
    // Menim.
    var stepIndex = steps.length;
    var step = new Step(stepIndex);
    var n = parseInt($(this).val());
    step.setItems(step.items, n);
    renderStep(step);
    steps.push(step);
});

// Computed variables.
var rowsPerStep;
computeRowsPerStep();

function renderStep(step) {
    // Render table.
    var htmlTable = htmlSampleTable.clone();
    htmlTable.attr('step-index', step.index);

    if (showStepInfoRow) {
        var tr = renderInfoRow(step);
        htmlTable.append(tr);
    }
    if (showNumberRow) {
        var tr = renderNumberRow(step);
        htmlTable.append(tr);
    }
    if (showNRow) {
        var tr = renderNRow(step);
        htmlTable.append(tr);
    }
    if (showResultRow) {
        var tr = renderResultRow(step);
        htmlTable.append(tr);
    }
    if (showSelectbox) {
        var tr = renderStepSelector(step);
        htmlTable.append(tr);
    }

    visiblePart.append(htmlTable);
}

function computeRowsPerStep() {
    rowsPerStep = 0;
    if (showStepInfoRow) {
        rowsPerStep++;
    }
    if (showNumberRow) {
        rowsPerStep++;
    }
    if (showNRow) {
        rowsPerStep++;
    }
    if (showResultRow) {
        rowsPerStep++;
    }
}

function removeSteps(startIndex) {
    var stepsCount = steps.length;
    steps = steps.slice(0, startIndex + 1);
    for (var i = stepsCount - 1; i > startIndex; i--) {
        $('table[step-index=' + i + ']').remove();
    }
}

function renderStepSelector(step) {
    var stepIndex = step.index;
    var selectbox = htmlSelectbox.clone();
    selectbox.attr('step-index', stepIndex);
    for (var i = 1; i < biggestN; i++) {
        var option = $('<option></option>');
        option.attr('value', i);
        option.text(i);
        selectbox.append(option);
    }

    selectbox.on('change', function () {
        var thisStepIndex = parseInt($(this).attr('step-index'));
        removeSteps(thisStepIndex);
        var step = new Step(thisStepIndex + 1);
        var previousStep = steps[thisStepIndex];
        var n = $(this).val();

        step.setItems(previousStep.items, n);
        renderStep(step);
        steps.push(step);
    });

    var trSelectbox = sampleTrSelectbox.clone();
    trSelectbox.find('.td-selectbox').append(selectbox);
    return trSelectbox;
}

function renderResultRow(step) {
    var tr = htmlResultRow.clone();
    for (var i = 0; i < step.items.length; i++) {
        var td = htmlTd.clone();
        var collatzItem = step.items[i];
        var resultIndex = collatzItem.result;
        if (arr.hasOwnProperty(resultIndex)) {
            var resultNumber = arr[resultIndex].getNumber();
        } else {
            var resultNumber = noItem;
        }
        td.text(resultNumber);
        tr.append(td);
    }

    // Legend.
    var xPrev = 'x<sub>' + step.index + '</sub>';
    var xNext = 'x<sub>' + (step.index + 1) + '</sub>';
    var legendText = xNext + ' = (3' + xPrev + '+1) / 2<sup>n</sup>';
    tr.find('th.legend').html(legendText);
    return tr;
    return tr;
}

function renderNumberRow(step) {
    var tr = htmlNumberRow.clone();
    for (var i = 0; i < step.items.length; i++) {
        var td = htmlTd.clone();
        var collatzItem = step.items[i];
        var number = collatzItem.getNumber();
        td.text(number);
        tr.append(td);
    }

    // Legend.
    var legendText = 'x<sub>' + step.index + '</sub>';
    tr.find('th.legend').html(legendText);
    return tr;
}

function renderNRow(step) {
    var tr = htmlNRow.clone();
    for (var i = 0; i < step.items.length; i++) {
        var td = htmlTd.clone();
        var collatzItem = step.items[i];
        td.text(collatzItem.n);
        var tdClass = 'n-' + collatzItem.n;
        td.addClass(tdClass);
        tr.append(td);
    }
    return tr;
}

function renderInfoRow(step) {
    var tr = htmlInfoRow.clone();
    var th = tr.find('th.info-cell');
    var text;
    if (step.n === null) {
        text = 'Step 0 - all odd numbers';
    } else {
        text = 'Step ' + step.index + ': n = ' + step.n;
    }

    th.text(text);
    tr.append(th);
    return tr;
}



function addStep(n) {
    lastStep = computeNewStep();
    renderStep(lastStep);
}

function computeNewStep() {
    var newStep = [];
    for (var i = 0; i <= lastStep.length; i++) {
        var index = lastStep[i][resultKey];
        if (index >= maxPos) {
            newStep[i] = noitem;
            continue;
        }
        newStep[i] = arr[index];
    }
    lastStep = newStep;
}

class StepRenderer {
    constructor(items, index) {
        this.items = items;
        this.index = index;
    }
}

// Create and render initial step (step number 0).
var step1 = new Step(0);
step1.setItemsDirectly(arr);
renderStep(step1);
steps.push(step1);
