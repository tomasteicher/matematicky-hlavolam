function path() {

}

// Biggest power of number 2.
var biggestN = 8;
// Number with biggest known n.
var biggestNPos = 42;
// Biggest number, that we are willing to compute.
var maxPos = 100;
// Key strings.
var nKey = 'n';
var resultKey = 'results';
var indexKey = 'i';

class Step {
    constructor(index) {
        this.index = index;
        this.n = null;
    }
    setItems(previousItems, n) {
        console.debug(arr);
        this.items = [];
        for (var i = 0; i < previousItems.length; i++) {
            if (previousItems[i].n == n) {
                var resultI = previousItems[i].result;
                if (!arr.hasOwnProperty(resultI)) {
                    continue;
                }
                this.items.push(arr[resultI]);
            }
        }
        this.n = n;
    }
    setItemsDirectly(items) {
        this.items = items;
    }
}


class CollatzItem {
    constructor(i) {
        this.i = i;
        this.result = null;
    }
    getNumber() {
        return this.i * 2 + 1;
    }
}



function mapN() {
    // Powers of number 2.
    var mathPowers = [];

    // Compute powers of number 2.
    mathPowers[0] = 1;
    for (var i = 1; i <= biggestN; i++) {
        mathPowers[i] = mathPowers[i - 1] * 2;
    }

    // Init..
    var arr = [];

    for (var i = 0; i <= maxPos; i++) {
        arr[i] = new CollatzItem(i);
    }
    arr[biggestNPos].n = biggestN;

    // Compute N from known numbers.
    for (var n = 1; n <= biggestN; n++) {
        var powersN = mathPowers[n];
        var diffPos = biggestNPos - mathPowers[n - 1];
        while (diffPos >= 0) {
            arr[diffPos].n = n;
            diffPos -= powersN;
        }
        diffPos = biggestNPos + mathPowers[n - 1];
        while (diffPos <= maxPos) {
            arr[diffPos].n = n;
            diffPos += powersN;
        }
    }

    // Find first occurance of each N.
    var firstOccurances = [];
    for (var i = 0; i <= maxPos; i++) {
        n = arr[i].n;
        if (!firstOccurances.hasOwnProperty(n)) {
            firstOccurances[n] = i;
        }
    }

    // Compute results.
    for (var n = 1; n <= biggestN; n++) {
        var first = firstOccurances[n];
        // First occurance must be computed manually.
        var number = (first * 2) + 1;
        var power = mathPowers[n];
        var powerDiff = mathPowers[n];
        var result = (3 * number + 1) / power;

        if (!arr.hasOwnProperty(first)) {
            continue;
        }
        arr[first].result = (result - 1) / 2;
        var nextResult = arr[first].result + 3;
        var nextIndex = first + powerDiff;
        while (nextIndex < maxPos) {
            arr[nextIndex].result = nextResult;
            nextIndex += powerDiff;
            nextResult += 3;
        }
    }
    return arr;
}

function createStep(stepNumber, data) {
    var tr = $('<tr id="ns"></td>n<td></td></tr>');
    for (var i = 0; i < data.length; i++) {
        var tdNumber = fillCellNumber(i);
        var tdN = fillCellN(arr[i]);
        trNumbers.append(tdNumber);
        tr.append(tdN);
    }
    var selectorClone = stepSelector.clone();
    selectorClone.attr('step', stepNumber);
    tr.append(selectorClone);
    return tr;
}


var arr = mapN();
console.debug(arr);